//
// Created by mnowak on 05.06.18.
//

#include <ncurses.h>
#include <zconf.h>
#include "Car.h"

Car::Car(int lane, Highway *highway) {
    velocity = static_cast<__useconds_t>(60000 + rand() % 60000);
    position = 0;
    current_lane = lane;
    this->highway = highway;
    character = static_cast<char>(rand() % 26 + 'A');
    highway->lane_mutex[lane].lock();
    highway->lanes[lane][0] = this;
    highway->lane_mutex[lane].unlock();
    t = thread(&Car::run, this);
}

void Car::run() {
    while (!finished) {
        int curr_lane = current_lane;


        // jezeli samochod jest na autostradzie
        if (current_lane < 5) {

            // jezeli przejedzie kolo wjazdu na autostrade
            if (current_lane == 4 && position > 50 && position < 65) {
                highway->cv.notify_one();
            }
            highway->lane_mutex[curr_lane].lock();

            // jezeli nie jest na ostatnim pasie
            if (current_lane > 0) {
                highway->lane_mutex[curr_lane - 1].lock();
                // jezeli musi i moze wyprzedzac
                if (need_to_pass() && can_pass()) {
                    //wyprzedza
                    pass();
                }
                highway->lane_mutex[curr_lane - 1].unlock();
            }

            //jezeli jest na pierwszym pasie i moze zjechac z autostrady
            if (current_lane == 4 && position == 112 && can_leave_highway()) {
                int r = rand() % 101;
                if (r < 40) {
                    leave_highway();
                }
            }

            // jezeli nie stoi przed nim samochod lub nie dojechal do konca
            if (can_move_forward()) {
                move_forward();
                //jezeli dojechal do konca
            } else if (position == highway->lanes[current_lane].size() - 1) {
                highway->lanes[current_lane][position] = nullptr;
                finished = true;
            }
            highway->lane_mutex[curr_lane].unlock();
        }


            // jezeli dojezdza do autostrady
        else if (current_lane == 5) {
            unique_lock lk(highway->ul_mutex);
            highway->lane_mutex[curr_lane].lock();

            if (can_move_forward()) {
                move_forward();
            }
                //jesli dojechal do autostrady
            else if (position == highway->lanes[current_lane].size() - 1) {
                if (!can_join_traffic()) {
                    highway->cv.wait(lk, [this]() { return can_join_traffic(); });
                }
                highway->lane_mutex[curr_lane - 1].lock();
                // jezeli moze wjechac na autostrade
                if (can_join_traffic()) {
                    join_traffic();

                }
                highway->lane_mutex[curr_lane - 1].unlock();
            }
            highway->lane_mutex[curr_lane].unlock();
            lk.unlock();
        }


            // jezeli zjezdza z autostrady
        else {
            highway->lane_mutex[curr_lane].lock();

            if (can_move_forward()) {
                move_forward();
            } else if (position == highway->lanes[current_lane].size() - 1) {
                highway->lanes[current_lane][position] = nullptr;
                finished = true;
            }
            highway->lane_mutex[curr_lane].unlock();
        }

        highway->print_mutex.lock();
        print();
        highway->print_mutex.unlock();
        usleep(velocity);
    }
}

void Car::leave_highway() {
    highway->lanes[current_lane + 2][0] = this;
    highway->lanes[current_lane][position] = nullptr;

    current_lane += 2;
    position = 0;
}

bool Car::can_move_forward() {
    return position < highway->lanes[current_lane].size() - 1 &&
           highway->lanes[current_lane][position + 1] == nullptr;
}

void Car::move_forward() {
    highway->lanes[current_lane][position + 1] = highway->lanes[current_lane][position];
    highway->lanes[current_lane][position] = nullptr;

    position++;
}

void Car::print() {
    if (current_lane < 5) {
        for (int i = 0; i < highway->lanes[current_lane].size(); i++) {
            if (highway->lanes[current_lane][i] != nullptr)
                mvaddch(current_lane * 2 + 1, i, highway->lanes[current_lane][i]->character);
            else
                mvaddch(current_lane * 2 + 1, i, ' ');
        }
    }

    for (int i = 0; i < highway->lanes[5].size(); i++) {
        if (highway->lanes[5][i] != nullptr)
            mvaddch(22 - i, 57, highway->lanes[5][i]->character);
        else {
            mvaddch(22 - i, 57, ' ');
        }
    }

    for (int i = 0; i < highway->lanes[6].size(); i++) {
        if (highway->lanes[6][i] != nullptr)
            mvaddch(11 + i, 112, highway->lanes[6][i]->character);
        else {
            mvaddch(11 + i, 112, ' ');
        }
    }

    refresh();
}

bool Car::need_to_pass() {
    return position < highway->lanes[current_lane].size() - 2 && (
            highway->lanes[current_lane][position + 1] != nullptr ||
            highway->lanes[current_lane][position + 2] != nullptr);
}

bool Car::can_pass() {

    return current_lane != 0 && highway->lanes[current_lane - 1][position] == nullptr;

}

void Car::pass() {
    highway->lanes[current_lane - 1][position] = this;
    highway->lanes[current_lane][position] = nullptr;

    current_lane--;

}

bool Car::can_join_traffic() {
    for (int i = 40; i < 61; i++) {
        if (highway->lanes[current_lane - 1][i] != nullptr)
            return false;
    }
    return true;
}

void Car::join_traffic() {
    highway->lanes[current_lane - 1][57] = this;
    highway->lanes[current_lane][position] = nullptr;

    current_lane--;
    position = 57;
}

bool Car::can_leave_highway() {
    return highway->lanes[6][0] == nullptr;
}
