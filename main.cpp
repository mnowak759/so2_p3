#include <iostream>
#include <ncurses.h>
#include "Highway.h"

int main() {
    initscr();
//   curs_set(0);

    for (int i = 0; i < 210; i++) {
        mvaddch(0, i, '-');
        mvaddch(2, i, '-');
        mvaddch(4, i, '-');
        mvaddch(6, i, '-');
        mvaddch(8, i, '-');
        mvaddch(10, i, '-');
    }
    for (int i = 0; i < 12; i++) {
        mvaddch(i + 11, 55, '*');
        mvaddch(i + 11, 59, '*');
        mvaddch(i + 11, 110, '*');
        mvaddch(i + 11, 114, '*');
    }

    Highway *highway = new Highway();
    refresh();
    getch();
    endwin();
    return 0;
}
