//
// Created by mnowak on 05.06.18.
//

#ifndef SO2_P3_HIGHWAY_H
#define SO2_P3_HIGHWAY_H

class Car;

#include "Car.h"
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

class Highway {
public:
    bool end = false;
    vector<vector<Car *>> lanes;
    mutex ul_mutex;
    condition_variable cv;
    thread create_car_thread;
    array<mutex, 7> lane_mutex;
    mutex print_mutex;

    void create_car();

    Highway();
};


#endif //SO2_P3_HIGHWAY_H
