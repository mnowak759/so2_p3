//
// Created by mnowak on 05.06.18.
//

#ifndef SO2_P3_CAR_H
#define SO2_P3_CAR_H

class Highway;

#include "Highway.h"
#include "thread"

using namespace std;

class Car {
public:
    bool finished = false;
    int current_lane;
    char character;
    int position;
    Highway *highway;
    thread t;
    __useconds_t velocity;

    void run();

    Car(int lane, Highway *highway);

    bool can_move_forward();

    bool can_pass();

    bool need_to_pass();

    void move_forward();

    void pass();

    void print();

    bool can_join_traffic();

    void join_traffic();

    void leave_highway();

    bool can_leave_highway();
};


#endif //SO2_P3_CAR_H
