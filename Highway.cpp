//
// Created by mnowak on 05.06.18.
//

#include <zconf.h>
#include <curses.h>
#include "Highway.h"

Highway::Highway() {
    lanes.resize(7);
    for (int i = 0; i < 5; i++)
        lanes[i].resize(210, nullptr);
    lanes[5].resize(12, nullptr);
    lanes[6].resize(12, nullptr);
    create_car_thread = thread(&Highway::create_car, this);
}

void Highway::create_car() {
    while (!end) {
        int i = rand() % 6;
        if (lanes[i][0] == nullptr) {
            auto *car = new Car(i, this);
        }
        __useconds_t time = 599999;
        usleep(time);
    }
}
